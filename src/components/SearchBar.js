import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';



const SearchBar = ({ term, onTermChange, onTermSubmit }) => {
    return(
       <>
        <View style={ styles.backgroundStyle }>
            <FontAwesome name="search" style={styles.iconStyle} />
            <TextInput 
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.inputStyle} 
            placeholder="Search"
            placeholderTextColor= '#3ee4c4'
            value={term}
            onChangeText={onTermChange}
            onEndEditing={onTermSubmit}
            />
        </View>
        </>
    );
};

const styles = StyleSheet.create ({
    backgroundStyle: {
        marginTop: 10, 
        backgroundColor: '#F0EEEE',
        height: 40,
        borderRadius: 5,
        marginHorizontal: 50,
        flexDirection: 'row',
        marginBottom: 10
    },
    inputStyle: {
        flex: 1,
        fontSize: 18,

    },
    iconStyle: {
        fontSize: 20,
        alignSelf: 'center',
        marginHorizontal: 15,
        color: '#3ee4c4'
    },

});


export default SearchBar;