import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import ProgressBar from '../components/ProgressBar';

const ResultsDetail = ({ result }) => {
    return (
        <View style={styles.container}>
            <Image style={styles.image} source={result.image_url ? { uri: result.image_url } : null} />
            <Text style={styles.name}>{result.name} </Text>     
            <Text style={styles.miles}>
            {result.rating} miles away
            </Text> 
            <Text style={styles.progresspoints}> 73 points until next Prize </Text>
            <ProgressBar style={styles.progressbarstyling} />
        </View>
    );
};

const styles = StyleSheet.create({
    image: {
        left: 5,
        top: 5,
        bottom: 5,
        width: 100,
        height: 100,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        marginBottom: 5,
        overflow: 'hidden',
        zIndex:-10,
    },
    name: {
        position: 'absolute',
        top: 5,
        left: 120,
        right: 20,
        height: 100,
        fontWeight: 'bold',
        fontSize: 18,
        color: '#000000',
        
    },
    miles: {
        position: 'absolute',
        top: 30,
        left: 120,
        width: 400,
        height: 100,
        fontSize: 12,
        color: '#808080',
    },
    container: {
        marginLeft: 0,
        marginBottom: 5
    },
    progressbarstyling: {
        position: 'absolute',
        top: 70,
        left: 120,
        right: 20
    },
    progresspoints: {
        position: 'absolute',
        textAlign: 'center',
        top: 50,
        left: 145,
        right: 45,
        height: 100,
        fontSize: 14,
        color: '#ef456f',   
    }
});
export default ResultsDetail