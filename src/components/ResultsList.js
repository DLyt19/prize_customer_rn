import React from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import ResultsDetail from './ResultsDetail';
import { withNavigation } from 'react-navigation';


const ResultsList = ({ title, results, navigation }) => {
    if (!results.length) {
        return null;
    }


    return (
        <View styles= {styles.spacer} > 
            <FlatList 
                showsVerticalScrollIndicator={false}
                data={results}
                keyExtractor={(result) => result.id}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity onPress={() => navigation.navigate('BusinessInfo', {id: item.id })}>
                            <View style={[styles.shadow, styles.container]}>
                                <ResultsDetail result={item} />
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create ({
    title: 
    {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginBottom: 5
    },
    shadow:
    {
        shadowColor: 'red',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 6,
        zIndex: -100,
        elevation: 10, //needed for android
    },
    container:
    {
        flexDirection: 'column',
        paddingBottom: 0,
        marginTop: 10,
        marginBottom: 0,
        marginHorizontal: 10,
        backgroundColor: '#ffffff',
        borderRadius: 5,
        zIndex: -10,
        shadowColor: 'red',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 6,
        zIndex: -100,
        elevation: 10, //needed for android
    },
    spacer:
    {
        marginVertical: 2
    },
    });
    

export default withNavigation(ResultsList);