import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView, SafeAreaView } from 'react-native';
import SearchBar from '../components/SearchBar';
import yelp from '../api/yelp';
import useResults from '../hooks/useResults';
import ResultsList from '../components/ResultsList';
import * as Font from 'expo-font';


const DiscoverScreen = () => {
    const [term, setTerm] = useState('');
    const [searchApi, results, errorMessage] = useResults ();
    
    
    const filterResults = (price) => {
        //price === '$' || '$$' || '$$$'
        return results.filter(result => {
            return result.price === price;
        });
    };


    return (
        <>  
            <View  style={ styles.searchBarContainer } >
            <SearchBar 
            term={term} 
            onTermChange={setTerm}
            onTermSubmit={() => searchApi(term)}
            />
            </View>
            {errorMessage ? <Text>{errorMessage}</Text>: null}
                <ScrollView style={ styles.MainContainer } > 
                    <ResultsList 
                        results={filterResults('$')} 
                    />
                </ScrollView>
        </>
    )

};

DiscoverScreen.navigationOptions = {
    title: 'Businesses Near You',
    headerTitleStyle: {
        textAlign: "center",
        alignSelf: "flex-start",
        flex:1,
        fontFamily: 'Roboto',
        fontSize: 32,
        color: '#ffffff',
    },
    headerStyle: {
        backgroundColor: '#40e5c5',
        height: 35,
        elevation: 0,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
        },
        shadowColor: 'transparent',
        borderBottomWidth: 0,
    }
};




const styles = StyleSheet.create({
    number: {
        marginLeft:15
    },
    searchBarContainer: {
        height: 60,
        backgroundColor:'#3ee4c4'
    },
    MainContainer:{
        flex: 1,
        backgroundColor: '#ebfcf9'
    }
});



export default DiscoverScreen;

