import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';
import yelp from '../api/yelp';
import { Ionicons } from '@expo/vector-icons';


const BusinessInfoScreen = ({ navigation }) => {
    const [result, setResult] = useState(null);
    const id = navigation.getParam('id');

    const getResult = async (id) => {
        const response = await yelp.get(`/${id}`);
        setResult(response.data);
    };
    useEffect(() => {
        getResult(id);
    },[]);

    if(!result) {
        return null;
    }

    return (
        <View>
            <Text>{result.name}</Text>
            <FlatList
                data={result.photos}
                keyExtractor={(photo) => photo}
                renderItem={({ item  }) => {
                    return <Image style={styles.image} source={{ uri: item }} />
                }} 
            />
        </View>
    );
};

BusinessInfoScreen.navigationOptions = {
    title: 'Info',
    headerTitleStyle: {
        textAlign: "center",
        alignSelf: "flex-start",
        flex:1,
        fontFamily: 'Roboto',
        fontSize: 32,
        color: '#ffffff',
    },
    headerStyle: {
        backgroundColor: '#40e5c5',
        height: 45,
        elevation: 0,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
        },
        shadowColor: 'transparent',
        borderBottomWidth: 0,
    },
    headerTitleContainerStyle: {
      left: 0, // THIS RIGHT HERE
      

    },
    headerLayoutPreset: 'center',
};

const styles = StyleSheet.create({
    image: {
        height:200,
        width:300
    }
});

export default BusinessInfoScreen;