import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { SafeAreaView } from 'react-navigation';
import ProgressBar from '../components/ProgressBar';


const FavoritesScreen = () => {
    return (
        <SafeAreaView forceInset={{ top: 'always' }} >
            <View>
                <Text style={{ fontSize: 48}}>Favorites Screen</Text>
            </View>
            </SafeAreaView>
    );
};

FavoritesScreen.navigationOptions = {
    title:'Favorites',
    tabBarIcon: ({ tintColor }) => ( <FontAwesome name="star" size={20} color={tintColor} />
    )
}


const styles = StyleSheet.create({});

export default FavoritesScreen;