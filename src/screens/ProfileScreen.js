import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { SafeAreaView } from 'react-navigation';
import CheckBox from 'react-native-check-box'



const ProfileScreen = () => {
    return (
        <SafeAreaView forceInset={{ top: 'always' }} >
        <View>
            <Text style={{ fontSize: 48}}>Profile Screen</Text>
        </View>
        </SafeAreaView>
    );
};

ProfileScreen.navigationOptions = {
    title:'Profile',
    tabBarIcon: ({ tintColor }) => ( <FontAwesome name="user" size={20} color={tintColor} />
    )
}




const styles = StyleSheet.create({});

export default ProfileScreen;