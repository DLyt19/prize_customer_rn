import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { 
  createAppContainer, 
  createSwitchNavigator
} from 'react-navigation';
import DiscoverScreen from './src/screens/DiscoverScreen';
import BusinessInfoScreen from './src/screens/BusinessInfoScreen';
import SearchBar from './src/components/SearchBar';
import React from 'react';
import SignupScreen from './src/screens/SignupScreen';
import SigninScreen from './src/screens/SigninScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import FavoritesScreen from './src/screens/FavoritesScreen';
import { FontAwesome } from '@expo/vector-icons';



const DiscoverFlow = createStackNavigator({
  Discover: DiscoverScreen,
  BusinessInfo: BusinessInfoScreen
})

DiscoverFlow.navigationOptions = {
  title: 'Discover',
  tabBarIcon: ({ tintColor }) => ( <FontAwesome name="search" size={20} color={tintColor} />
  ),
};

const switchNavigator = createSwitchNavigator({
  loginFlow: createStackNavigator({
      Signup: SignupScreen,
      Signin: SigninScreen
    }),
    mainFlow: createBottomTabNavigator({
      DiscoverFlow,
      Favorites: FavoritesScreen,
      Profile: ProfileScreen 
      },
      {
        tabBarOptions: {
          showLabel: true,
          showIcon: true,
          
          activeTintColor: '#3ee4c4',  //this changes the text on tabBar only
          inactiveTintColor: '#ffffff', //this changes the text on tabBar only
          labelStyle: {
            fontSize: 14,
            fontFamily: 'Roboto'
          },
          style: {
            backgroundColor: 'black',
            height: 52.5,
            padding: 5,
            borderTopWidth:0,
            borderTopColor: '#000000'
          },
        }
        }
      ),

      
  })

export default createAppContainer (switchNavigator);

